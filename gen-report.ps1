Param($outputPath="report.xml")

[xml]$doc = New-Object System.Xml.XmlDocument;

$client_dir = "C:\Users\David\Documents\Programming\test_folders"

class ClientRow {
	[string]$client
	[string]$integration
	[string]$connect_product
	[string]$imu
	
	[string]$emails
	[string]$folders
	
	[string]$import
	[string]$export
	
	[string]$task_name
	
	[string]$last_run
	[string]$next_run
	[string]$status
	
	[System.Xml.XmlElement] toNode([xml]$doc) {
		$root = $doc.CreateNode("element", "client", $null);
		$root.SetAttribute("name", $this.client);
		
		$integration_node = $doc.CreateNode("element", "integration", $null);
		$integration_node.SetAttribute("name", "$($this.integration) $($this.imu)");
		$root.AppendChild($integration_node);
		
		$con_prod_node = $doc.CreateElement("connect");
		$con_prod_node.InnerText = $this.connect_product;
		
		$integration_node.AppendChild($con_prod_node);
		
		$emails_node = $doc.CreateNode("element", "emails", $null);
		foreach($email in $this.emails.split(",")) {
			$email_element = $doc.CreateElement("email");
			$email_element.InnerText = $email.Trim();
			
			$emails_node.AppendChild($email_element) | Out-Null;
		}
		
		$integration_node.AppendChild($emails_node) | Out-Null;
		
		$folders_node = $doc.CreateNode("element", "folders", $null);
		
		foreach($folder in $this.folders.split(",")) {
			if([string]::isNullOrEmpty($folder)) {
				continue
			}
			$folder_element = $doc.CreateElement("folder");
			$folder_element.InnerText = $folder;
			
			$folders_node.AppendChild($folder_element) | Out-Null;
		}
		
		$integration_node.AppendChild($folders_node);
		
		$export_node = $doc.CreateElement("run-exports");
		$export_node.InnerText = $this.export;
		
		$import_node = $doc.CreateElement("run-imports");
		$import_node.InnerText = $this.import;
		
		$integration_node.AppendChild($import_node) | Out-Null;
		$integration_node.AppendChild($export_node) | Out-Null;
		
		$status_node   = $doc.CreateElement("schedule-status");
		$last_run_node = $doc.CreateElement("last-run");
		$next_run_node = $doc.CreateElement("next-run");
		
		$status_node.InnerText   = $this.status;
		$last_run_node.InnerText = $this.last_run;			
		$next_run_node.InnerText = $this.next_run;
		
		$integration_node.AppendChild($status_node)   | Out-Null;
		$integration_node.AppendChild($next_run_node) | Out-Null;
		$integration_node.AppendChild($last_run_node) | Out-Null;

		return $root;
	}
}

#This expects the path to be <CLIENT_DIR>/<CLIENT>/<INTEGRATION>/
function create([string]$path) {
	$integration = (Split-Path (Get-Item $path) -leaf);
	$client      = (Get-Item $path).parent;
	
	$imus = (Get-ChildItem $path -Name -Directory) -match "unanet_imu";

	$ret_list = New-Object System.Collections.Generic.List[System.Object];
	foreach($imu in $imus) {
		$row = [ClientRow]::new();
		
		$row.client = $client;
		$row.integration = $integration;
		$row.imu = $imu;
		
		$connect_props = (ConvertFrom-StringData (Get-Content(Join-Path $path "hosted_imu_config.txt") | Out-String ));
        if($connect_props.monitoring -match "false") {
            continue;
        }

		$full_imu_path = Join-Path $path $imu
		
		if($props.connect -match "true") {
			$row.connect_product = "Y";
		}else{
			$row.connect_product = "N";
		}
		
		$email_props_path = Join-Path $full_imu_path "imu_config.properties"
		$props = (ConvertFrom-StringData (Get-Content $email_props_path | Out-String ));
		
		$row.emails = $props.'email.to';
		$row.import = $props.'run.imports';
		$row.export = $props.'run.exports';
		
		$row.task_name = getTask($row);
		
		$folder_loc = Join-Path $full_imu_path "data"
		$folders = Get-ChildItem $folder_loc | Sort-Object -Property Name -Descending
		
		$row.folders = ""
		
		for($i = 0; $i -lt [math]::min($folders.Length, 4); $i++) {
			$row.folders = "{0}{1}{2}" -f $row.folders, ",", $folders[$i].Name
		}
		
		if(![string]::isNullOrEmpty($row.task_name)) {
			$row.status    = (Get-ScheduledTask     -TaskName $row.task_name).State
			$row.next_run  = (Get-ScheduledTaskInfo -TaskName $row.task_name).NextRunTime
			$row.last_run  = (Get-ScheduledTaskInfo -TaskName $row.task_name).LastRunTime
		}else{
			$row.status =   "N/A"
			$row.next_run = "N/A"
			$row.last_run = "N/A"
		}
		
		$ret_list.Add($row) | Out-Null;
	}
	
	return $ret_list;
}

function getTask([ClientRow]$entry) {
    $occurence = $entry.imu.LastIndexOf('_') + 1;
    $temp = $entry.imu.Substring($occurence, $entry.imu.Length - $occurence);

	$action_path = (Join-Path $entry.client $entry.integration) + '*' + $entry.imu
	
	#This retrieves the task from the associated batch filename
	#a true work of art
	$task =
		Get-ScheduledTask -TaskPath "\" | 
		Select-Object * |
		Where-Object { $_.Actions | Where-Object { $_.Execute -like "*$($action_path)*" } }[0];
		
	
	return $task.TaskName
}

$doc.AppendChild($doc.CreateXmlDeclaration("1.0", "UTF-8", $null)) | Out-Null;
$doc.AppendChild($doc.CreateComment((Get-Date -Format g))) | Out-Null;

$root = $doc.CreateNode("element", "record", $null);
$doc.AppendChild($root) | Out-Null;

# Find each client, iterating through the directories creating an entry for them
foreach($folder in (Get-ChildItem $client_dir -Directory -Name)) {
	foreach($sub_folder in (Get-ChildItem(Join-Path $client_dir $folder) -Directory -Name)) {
		if(!($sub_folder -match "-STAGE")) {
			$path = Join-Path(Join-Path $client_dir $folder) $sub_folder;
			$integrations = create($path);
			
			foreach($integration in $integrations) {
				$root.AppendChild($integration.toNode($doc)) | Out-Null;
			}
		}
	}
}

$doc.save($outputPath);